import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { SanitizerPipe } from './pipes/sanitizer.pipe';
import { LoginPageComponent } from './component/login-page/login-page.component';
import { MovieDashboardComponent } from './component/movie-dashboard/movie-dashboard.component';
import { MoivesModalComponent } from './component/modal/moives-modal/moives-modal.component';
import { LoaderComponent } from './component/modal/loader/loader.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatPaginatorModule } from '@angular/material/paginator';

@NgModule({
  declarations: [
    AppComponent,
    SanitizerPipe,
    LoginPageComponent,
    MovieDashboardComponent,
    MoivesModalComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,

    BrowserAnimationsModule,
    MatButtonModule,
    MatDialogModule,
    MatPaginatorModule,
  ],
  providers: [],
  entryComponents: [MoivesModalComponent, LoaderComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
