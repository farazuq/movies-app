import { ServiceService } from './../../services/service.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  userDetailes: FormGroup;
  invalidFormData = false;
  showLoader = false;
  invalidCred = false;
  constructor(private router: Router, private service: ServiceService) {
    this.userDetailes = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
  }

  ngOnInit(): void {}

  login() {
    if(this.userDetailes.invalid) {
      this.invalidFormData = true;
      this.invalidCred = false;
      return this.userDetailes.markAllAsTouched();
    }
    this.showLoader = true;
    this.service.login(this.userDetailes.value).subscribe((res: any) => {
      this.invalidFormData = false;
      this.invalidCred = false;
      this.showLoader = false;
      localStorage.setItem('Token', JSON.stringify(res.data.token));
      this.router.navigate(['/dashboard']);
    }, (err: any) => {
      console.log('error', err);
      this.userDetailes.reset();
      this.invalidFormData = true;
      this.invalidCred = true;
      this.showLoader = false;
    });
  }
}
