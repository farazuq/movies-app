import { MoivesModalComponent } from './../modal/moives-modal/moives-modal.component';
import { ServiceService } from './../../services/service.service';
import { Component, OnInit } from '@angular/core';
import { urlConstants } from '../../services/urlConstant';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

@Component({
  selector: 'app-movie-dashboard',
  templateUrl: './movie-dashboard.component.html',
  styleUrls: ['./movie-dashboard.component.scss']
})
export class MovieDashboardComponent implements OnInit {
  moviesDetails:any = [];
  searchArr:any = [];
  totalCount: any;
  initialUrl = `${urlConstants.baseUrl}maya/movies/`;
  nextUrl: any;
  previousUrl: any;

  constructor(private service: ServiceService, public matDialog: MatDialog) { }

  ngOnInit(): void {
    this.getMoviesList(this.initialUrl);
  }

  getMoviesList(url?: any) {
    this.service.loader();
    this.service.getMoviesList(url).subscribe((res: any) => {
      this.service.dismissLoader();
      if((this.previousUrl != res.next)) {
        this.searchArr = [...this.searchArr, ...res.results];
      }
      this.totalCount = res.count;
      this.previousUrl = res.previous;
      this.nextUrl = res.next;
      this.moviesDetails = res.results;
    }, (err: any) => {
      console.log('error', err);
      this.service.dismissLoader();
    });
  }

  getImage(name: any) {
    return `${urlConstants.imgUrl}${name}`;
  }

  openModal(data: any) {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component";
    dialogConfig.height = "auto";
    dialogConfig.width = "100%";
    dialogConfig.data = data;
    const modalDialog = this.matDialog.open(MoivesModalComponent, dialogConfig);
  }

  searchData(ev: any) {
    let searchText = ev.target.value;
    setTimeout(() => {
      this.moviesDetails = this.searchArr.filter((e: any) => {
        let title = e.title.toLowerCase();
        if(title.includes(searchText.toLowerCase())) {
          return true;
        } else {
          return false;
        }
      });
    }, 250);
  }

  pageChange(ev: any) {
    if(ev.previousPageIndex > ev.pageIndex) {
      this.getMoviesList(this.previousUrl);
    } else {
      this.getMoviesList(this.nextUrl);
    }
  }

  logout() {
    this.service.logout();
  }

}
