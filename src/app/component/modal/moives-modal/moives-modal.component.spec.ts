import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MoivesModalComponent } from './moives-modal.component';

describe('MoivesModalComponent', () => {
  let component: MoivesModalComponent;
  let fixture: ComponentFixture<MoivesModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MoivesModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MoivesModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
