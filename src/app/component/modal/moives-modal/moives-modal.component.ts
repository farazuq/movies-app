import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { urlConstants } from '../../../services/urlConstant';

@Component({
  selector: 'app-moives-modal',
  templateUrl: './moives-modal.component.html',
  styleUrls: ['./moives-modal.component.scss']
})
export class MoivesModalComponent implements OnInit {

  moviesDetails: any;
  constructor(public dialogRef: MatDialogRef<MoivesModalComponent>) { }

  ngOnInit(): void {
    this.moviesDetails = this.dialogRef._containerInstance._config.data;
  }

  getImage(name: any) {
    return `${urlConstants.imgUrl}${name}`;
  }

  closeModal() {
    this.dialogRef.close();
  }

}
