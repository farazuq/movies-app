import { DomSanitizer } from '@angular/platform-browser';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sanitizer'
})
export class SanitizerPipe implements PipeTransform {

  constructor(private sanitization: DomSanitizer) {}
  transform(value: any): any {
    return this.sanitization.bypassSecurityTrustUrl(value);
  }

}
