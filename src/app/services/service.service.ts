import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { urlConstants } from '../services/urlConstant';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { LoaderComponent } from '../component/modal/loader/loader.component';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http: HttpClient, private router: Router, public matDialog: MatDialog) { }

  login(loginData: any) {
    return this.http.post(`${urlConstants.baseUrl}usermodule/login/`, loginData);
  }

  getMoviesList(url: any) {
    let authorizationToken: any = localStorage.getItem('Token');
    return this.http.get(url, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: JSON.parse(authorizationToken)
       }),
    });
  }

  loader() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.id = "modal-component-loader";
    this.matDialog.open(LoaderComponent, dialogConfig);
  }

  dismissLoader() {
    this.matDialog.closeAll()
  }

  isUserLogin() {
    let isUserLogin = localStorage.getItem('Token');
    return isUserLogin? true : false;
  }

  logout() {
    localStorage.removeItem('Token');
    this.router.navigate(['/login']);
  }
}
