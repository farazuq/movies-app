import { MovieDashboardComponent } from './component/movie-dashboard/movie-dashboard.component';
import { LoginPageComponent } from './component/login-page/login-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RedirectGuard } from './guard/redirect.guard';
import { AuthGuard } from './guard/auth.guard';

const routes: Routes = [
  {path: '', redirectTo:'login', pathMatch: 'full'},
  {path: 'login', component: LoginPageComponent, canActivate: [RedirectGuard]},
  {path: 'dashboard', component: MovieDashboardComponent, canActivate: [AuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
